@extends('layouts.app')

@section('content')

<div id="app">
    <div class="container">
        <draggable :list="list" class="dragArea row">
            <card-component v-for="element in list" :key="element.name" :class="element.size">
                @{{element.name}}
            </card-component>
        </draggable>
    </div>
</div>

@endsection
